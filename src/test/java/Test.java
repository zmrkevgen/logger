import Other.Constants;
import Other.Listener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.annotations.BeforeTest;



public class Test {
    WebDriverEventListener driver;
    @BeforeTest
    public void projectSettings() {
        System.setProperty("webdriver.chrome.driver", "D://Java for AQA//orgwikipediaen//src//main//resources//chromedriver.exe");


    }
    @org.testng.annotations.Test
    public void firstTest(){
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(new ChromeDriver());
        Listener handler = new Listener();
        eventFiringWebDriver.register(handler);


        eventFiringWebDriver.get("https://en.wikipedia.org/wiki/Main_Page");
        WebElement element = eventFiringWebDriver.findElement(By.xpath("//div[@id='p-logo']"));
        element.isDisplayed();
        eventFiringWebDriver.close();


    }
    }
